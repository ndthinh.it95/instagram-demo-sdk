# INSTAGRAM DEMO SDK

Javascript SDK for INSTAGRAM DEMO API, it is used for Web App, React Native app.

## SETUP SDK

### Configure authentication

Create a deploy token with both permission: `read_package_registry` and `write_package_registry`

Create a `.npmrc` file and put these lines:

```text
@ndthinh.it95:registry=https://gitlab.com/api/v4/projects/[PROJECT_ID]/packages/npm/
//gitlab.com/api/v4/projects/[PROJECT_ID]/packages/npm/:_authToken=[DEPLOY_TOKEN]
```

### Publish a package

- Ensure authentication is configured (using `.npmrc`)
- Update version in `package.json`
- Package is release and publish automatically by CI/CD but you can also publish from local using `npm publish` command.

## Usage

### Install a package

- Ensure authentication is configured (using `.npmrc`)

```bash
npm i @ndthinh.it95/instagram-demo-sdk
```

### Configuration

```javascript
import { Api, createApiClient } from '@ndthinh.it95/instagram-demo-sdk';

const config = {
  baseUrl: 'https://instagram-scraper-api2.p.rapidapi.com/v1',
};

const client = createApiClient(config);
const api = new Api(client);
```

## Install packages & Build libs folder

```bash
npm install --force
npm run build
```

## Testing

```bash
npm run test
```
