import { expect } from 'chai';
import { describe, it } from 'mocha';
import { api } from '../config.test';
import { IPagination } from '../../libs/pagination';
import { IUser } from '../../libs/api/user/models';

describe('Index User', () => {
  it('Should get list of users successfully', async () => {
    const usernameInput = 'mrbeast';
    const users: IPagination<IUser> = await api.user.getUsers(usernameInput);

    expect(users).to.have.property('data');
    expect(users.data).to.have.property('count').that.is.a('number');
    expect(users.data).to.have.property('items').that.is.an('array');

    users.data.items.forEach((user) => {
      expect(user).to.have.property('id').that.is.a('string');
      expect(user).to.have.property('username').that.is.a('string');
      expect(user).to.have.property('full_name').that.is.a('string');
      expect(user).to.have.property('is_private').that.is.a('boolean');
      expect(user).to.have.property('is_verified').that.is.a('boolean');
      if (user.profile_pic_url) {
        expect(user).to.have.property('profile_pic_url').that.is.a('string');
      }
      if (user.profile_pic_url_hd) {
        expect(user).to.have.property('profile_pic_url_hd').that.is.a('string');
      }
    });
  });

  it('Should get a random user successfully', async () => {
    const user: IUser = await api.user.getRandomUser();
    expect(user).to.be.not.null;
  });
});
