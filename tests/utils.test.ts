const delay = (delay_time: number): Promise<void> => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, delay_time);
  });
};

export { delay };
