import { describe, it } from 'mocha';
import { api } from '../config.test';
import { IPagination } from '../../libs/pagination';
import { IPostReel, IReel, MediaItem, MediaType } from '../../libs/api/post/models';
import { expect } from 'chai';

describe('Index Post', () => {
  it('Should get list of posts successfully', async () => {
    const usernameInput = 'mrbeast';
    const posts: IPagination<IPostReel> = await api.post.getPostReel(usernameInput);

    // Expect the response to have a data field that is an array
    expect(posts.data).to.have.property('items').that.is.an('array');

    // Check that each item in the data array is a valid IPostReel object
    posts.data.items.forEach((item: IPostReel) => {
      expect(item).to.have.property('id').that.is.a('string');
      expect(item).to.have.property('user').that.is.an('object');
      expect(item.user).to.have.property('id').that.is.a('string');
      expect(item).to.have.property('caption').that.is.an('object');
      expect(item.caption).to.have.property('id').that.is.a('string');
      expect(item.caption).to.have.property('created_at').that.is.a('number');
      expect(item.caption).to.have.property('text').that.is.a('string');
      expect(item).to.have.property('like_count').that.is.a('number');
      expect(item).to.have.property('comment_count').that.is.a('number');
      expect(item).to.have.property('is_video').that.is.a('boolean');

      if (item.media_name === MediaType.ALBUM) {
        // Item is an IAlbum
        expect(item).to.have.property('media_name', MediaType.ALBUM);
        expect(item).to.have.property('carousel_media').that.is.an('array');
        item.carousel_media.forEach((mediaItem: MediaItem) => {
          expect(mediaItem).to.have.property('id').that.is.a('string');
          expect(mediaItem).to.have.property('is_video').that.is.a('boolean');
          if ('thumbnail_url' in mediaItem) {
            expect(mediaItem).to.have.property('thumbnail_url').that.is.a('string');
          }
          if ('video_url' in mediaItem) {
            expect(mediaItem).to.have.property('video_url').that.is.a('string');
          }
        });
      } else if (item.media_name === MediaType.REEL) {
        // Item is an IReel
        expect(item).to.have.property('media_name', MediaType.REEL);
        expect(item).to.have.property('video_url').that.is.a('string');
      } else if (item.media_name === MediaType.POST) {
        // Item is an IPost
        expect(item).to.have.property('media_name', MediaType.POST);
        expect(item).to.have.property('thumbnail_url').that.is.a('string');
      }
    });

    // Expect the response to have a pagination token if available
    if (posts.pagination_token) {
      expect(posts).to.have.property('pagination_token').that.is.a('string');
    }
  });

  it('Should get list of reels successfully', async () => {
    const usernameInput = 'mrbeast';
    const reels: IPagination<IReel> = await api.post.getReel(usernameInput);

    // Expect the response to have a data field that is an array
    expect(reels.data).to.have.property('items').that.is.an('array');

    // Check that each item in the data array is a valid IPostReel object
    reels.data.items.forEach((item: IPostReel) => {
      expect(item).to.have.property('id').that.is.a('string');
      expect(item).to.have.property('user').that.is.an('object');
      expect(item.user).to.have.property('id').that.is.a('string');
      expect(item).to.have.property('caption').that.is.an('object');
      expect(item.caption).to.have.property('id').that.is.a('string');
      expect(item.caption).to.have.property('created_at').that.is.a('number');
      expect(item.caption).to.have.property('text').that.is.a('string');
      expect(item).to.have.property('like_count').that.is.a('number');
      expect(item).to.have.property('comment_count').that.is.a('number');
      expect(item).to.have.property('is_video').that.is.a('boolean');
      expect(item).to.have.property('media_name', MediaType.REEL);
      expect(item).to.have.property('video_url').that.is.a('string');
    });

    // Expect the response to have a pagination token if available
    if (reels.pagination_token) {
      expect(reels).to.have.property('pagination_token').that.is.a('string');
    }
  });
});
