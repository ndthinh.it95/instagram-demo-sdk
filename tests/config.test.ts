import { Api, createApiClient } from '../libs';
import { ApiConfiguration } from '../libs/api/models';
import { API_HOST, API_KEY, BASE_URL } from './credentials.test';

const config: ApiConfiguration = {
  // Replace your credentials here
  baseUrl: BASE_URL,
  apiKey: API_KEY,
  apiHost: API_HOST,
  appVersion: '1.0.0',
  deviceId: '15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225',
  timeout: 30 * 1000,
  enableCache: true,
  cacheExpiresIn: 300,
};

export const client = createApiClient(config);
export const api = new Api(client);
