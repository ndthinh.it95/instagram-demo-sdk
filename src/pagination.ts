export interface IPaginationData<T> {
  count: number;
  items: Array<T>;
}

export interface IPagination<T> {
  data: IPaginationData<T>;
  pagination_token?: string;
}
