import axios, { AxiosResponse, AxiosRequestConfig, AxiosError, AxiosInstance, AxiosRequestHeaders } from 'axios';
import { ApiError } from './error';
import { setupCache } from 'axios-cache-adapter';
import { ApiConfiguration } from '../api/models';

export interface Params {
  [key: string]: unknown;
}

export type RequestInterceptor = (config: Partial<AxiosRequestConfig>) => Promise<AxiosRequestConfig>;
export type ResponseInterceptor = (response: AxiosResponse) => Promise<AxiosResponse>;

interface Interceptor {
  request?: RequestInterceptor;
  response?: ResponseInterceptor;
}
const TIMEOUT_REQUEST_DEFAULT = 1000 * 30; //30s
const CACHE_EXPIRES_DEFAULT = 5 * 60 * 1000; //300s

export class ApiClient {
  public axiosInstance: AxiosInstance;
  public constructor(protected apiConfig: ApiConfiguration) {
    const cache = setupCache({
      maxAge: apiConfig.cacheExpiresIn ? apiConfig.cacheExpiresIn * 1000 : CACHE_EXPIRES_DEFAULT,
      exclude: {
        query: false,
      },
    });
    const options = {
      baseURL: apiConfig.baseUrl,
      timeout: apiConfig.timeout ?? TIMEOUT_REQUEST_DEFAULT,
      withCredentials: true,
      headers: this.headers(),
      adapter: apiConfig.enableCache ? cache.adapter : undefined,
    };

    this.axiosInstance = axios.create(options);
  }

  private headers(): AxiosRequestHeaders {
    let headers = {};
    if (this.apiConfig.apiKey) {
      headers = { ...headers, 'x-rapidapi-key': this.apiConfig.apiKey };
    }
    if (this.apiConfig.apiHost) {
      headers = { ...headers, 'x-rapidapi-host': this.apiConfig.apiHost };
    }
    if (this.apiConfig.appVersion) {
      headers = { ...headers, 'App-Version': this.apiConfig.appVersion };
    }
    if (this.apiConfig.deviceId) {
      headers = { ...headers, 'Device-Id': this.apiConfig.deviceId };
    }
    return headers;
  }

  public setApiConfig(apiConfig: ApiConfiguration): void {
    this.apiConfig = apiConfig;
  }

  public getApiConfig(): ApiConfiguration {
    return this.apiConfig;
  }

  private success<T>(response: AxiosResponse): T {
    return response.data;
  }

  private error(e: AxiosError): ApiError {
    if (e.response && e.response.data) {
      const errorCode = e.response.data.code || 'unknown';
      const message = e.response.data.error_detail || e.response.data.message || e.message;
      return new ApiError(message, errorCode);
    }
    return this.handleUnknownError(e);
  }

  private handleUnknownError(e: Error) {
    return new ApiError(e.message, 'unknown');
  }

  private uri(uri: string): string {
    return `${this.apiConfig.baseUrl}${uri}`;
  }

  private async configs(config?: AxiosRequestConfig): Promise<AxiosRequestConfig> {
    const headers = config?.headers || {};
    return { ...config, headers };
  }

  public addInterceptor(interceptor: Interceptor): void {
    if (interceptor.request) {
      this.addRequestInterceptor(interceptor.request);
    }
    if (interceptor.response) {
      this.addResponseInterceptor(interceptor.response);
    }
  }

  public addRequestInterceptor(interceptor: RequestInterceptor): void {
    this.axiosInstance.interceptors.request.use(interceptor);
  }

  public addResponseInterceptor(interceptor: ResponseInterceptor): void {
    this.axiosInstance.interceptors.response.use(interceptor);
  }

  public async get<T>(uri: string, params: Params = {}): Promise<T> {
    try {
      const config = await this.configs({ params });
      const r = await this.axiosInstance.get(this.uri(uri), config);
      return this.success<T>(r);
    } catch (e) {
      throw this.error(e as AxiosError);
    }
  }

  public async post<T, P = unknown>(uri: string, data?: P, params: Params = {}): Promise<T> {
    try {
      const config = await this.configs({ params });
      const r = await this.axiosInstance.post(this.uri(uri), data, config);
      return this.success<T>(r);
    } catch (e) {
      throw this.error(e as AxiosError);
    }
  }

  public async patch<T, P = unknown>(uri: string, data?: P, params: Params = {}): Promise<T> {
    try {
      const config = await this.configs({ params });
      const r = await this.axiosInstance.patch(this.uri(uri), data, config);
      return this.success<T>(r);
    } catch (e) {
      throw this.error(e as AxiosError);
    }
  }

  public async put<T, P = unknown>(uri: string, data?: P, params: Params = {}): Promise<T> {
    try {
      const config = await this.configs({ params });
      const r = await this.axiosInstance.put(this.uri(uri), data, config);
      return this.success<T>(r);
    } catch (e) {
      throw this.error(e as AxiosError);
    }
  }

  public async delete<T>(uri: string, params: Params = {}): Promise<T> {
    try {
      const config = await this.configs({ params });
      const r = await this.axiosInstance.delete(this.uri(uri), config);
      return this.success(r);
    } catch (e) {
      throw this.error(e as AxiosError);
    }
  }
}
