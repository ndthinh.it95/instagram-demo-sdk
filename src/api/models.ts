export interface ApiConfiguration {
  baseUrl: string;
  apiKey: string;
  apiHost: string;
  userAgent?: string;
  appVersion?: string;
  deviceId?: string;
  deviceName?: string;
  timeout?: number;
  enableCache?: boolean;
  cacheExpiresIn?: number; // second
  osVersion?: string;
  osName?: string;
  apiVersion?: string;
}
