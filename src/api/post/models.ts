import { IUser } from '../user/models';

export enum MediaType {
  ALBUM = 'album',
  REEL = 'reel',
  POST = 'post',
}

export interface IPostCaption {
  id: string;
  created_at: number;
  text: string;
}

export interface BaseMediaItem {
  id: string;
  is_video: boolean;
  thumbnail_url: string;
}

export type MediaImageItem = BaseMediaItem;
export type MediaVideoItem = BaseMediaItem & { video_url: string };
export type MediaItem = MediaImageItem | MediaVideoItem;

export interface BaseItem {
  id: string;
  user: IUser;
  caption: IPostCaption;
  like_count: number;
  comment_count: number;
  is_video: boolean;
  thumbnail_url: string;
}

export interface IAlbum extends BaseItem {
  media_name: MediaType.ALBUM;
  carousel_media: MediaItem[];
}

export interface IReel extends BaseItem {
  media_name: MediaType.REEL;
  video_url: string;
  share_count: number;
}

export interface IPost extends BaseItem {
  media_name: MediaType.POST;
}

export type IPostReel = IPost | IReel | IAlbum;
