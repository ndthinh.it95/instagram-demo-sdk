import { ApiClient } from '../../http/client';
import { IPagination } from '../../pagination';
import { IPostReel, IReel } from './models';

export class Post {
  public constructor(public readonly client: ApiClient) {}

  public async getPostReel(username_or_id_or_url: string, pagination_token?: string): Promise<IPagination<IPostReel>> {
    return this.client.get<IPagination<IPostReel>>('/v1.2/posts', {
      username_or_id_or_url,
      pagination_token,
    });
  }

  public async getReel(username_or_id_or_url: string, pagination_token?: string): Promise<IPagination<IReel>> {
    return this.client.get<IPagination<IReel>>('/v1.2/reels', {
      username_or_id_or_url,
      pagination_token,
    });
  }
}
