import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select } from 'redux-saga/effects';
import { IActionGetPostsPayload, IActionGetReelsPayload } from './models';
import * as PostService from './service';
import * as PostUtils from './utils';
import { saveIsFetchPosts, savePosts, saveReels, selectPostsRoot } from './reducer';
import { selectUserRoot } from '../../user/redux/reducer';

function* selectPostRoot() {
  const { username_or_id_or_url, posts, reels } = yield select(selectPostsRoot);
  return { username_or_id_or_url, posts, reels };
}

function* selectRandomUsername() {
  const { randomUser } = yield select(selectUserRoot);
  return { username: randomUser?.username };
}

function* doGetPostByInput(isLoadMore = false) {
  const { username_or_id_or_url, posts: oldPosts } = yield call(selectPostRoot);
  const token = isLoadMore ? oldPosts.pagination_token : undefined;
  const { result, error } = yield call(PostService.getPosts, username_or_id_or_url, token);
  if (result) {
    const { result: newPosts } = yield call(PostUtils.formatListPosts, oldPosts, result, isLoadMore);
    yield put(savePosts({ posts: newPosts }));
  }
  return { result, error };
}

function* doGetPostByRandom(isLoadMore = false) {
  const { username } = yield call(selectRandomUsername);
  const { posts: oldPosts } = yield call(selectPostRoot);
  const token = isLoadMore ? oldPosts.pagination_token : undefined;
  const { result, error } = yield call(PostService.getPosts, username, token);
  if (result) {
    const { result: newPosts } = yield call(PostUtils.formatListPosts, oldPosts, result, isLoadMore);
    yield put(savePosts({ posts: newPosts }));
  }
  return { result, error };
}

function* doGetReelByInput(isLoadMore = false) {
  const { username_or_id_or_url, reels: oldReels } = yield call(selectPostRoot);
  const token = isLoadMore ? oldReels.pagination_token : undefined;
  const { result, error } = yield call(PostService.getReels, username_or_id_or_url, token);
  if (result) {
    const { result: newReels } = yield call(PostUtils.formatListPosts, oldReels, result, isLoadMore);
    yield put(saveReels({ reels: newReels }));
  }
  return { result, error };
}

function* doGetReelByRandom(isLoadMore = false) {
  const { username } = yield call(selectRandomUsername);
  const { reels: oldReels } = yield call(selectPostRoot);
  const token = isLoadMore ? oldReels.pagination_token : undefined;
  const { result, error } = yield call(PostService.getReels, username, token);
  if (result) {
    const { result: newReels } = yield call(PostUtils.formatListPosts, oldReels, result, isLoadMore);
    yield put(saveReels({ reels: newReels }));
  }
  return { result, error };
}

export function* doGetPost(isLoadMore = false) {
  const { username_or_id_or_url } = yield call(selectPostRoot);
  if (username_or_id_or_url) {
    const { result, error } = yield call(doGetPostByInput, isLoadMore);
    return { result, error };
  } else {
    const { result, error } = yield call(doGetPostByRandom, isLoadMore);
    return { result, error };
  }
}

export function* getPostsFn(action: PayloadAction<IActionGetPostsPayload>) {
  const { isLoadMore = false, onSuccess, onFail } = action.payload;
  const { result, error } = yield call(doGetPost, isLoadMore);
  yield put(saveIsFetchPosts({ isFetchPosts: true }));
  if (result) onSuccess?.(result);
  else onFail?.(error);
}

export function* doGetReel(isLoadMore = false) {
  const { username_or_id_or_url } = yield call(selectPostRoot);
  if (username_or_id_or_url) {
    const { result, error } = yield call(doGetReelByInput, isLoadMore);
    return { result, error };
  } else {
    const { result, error } = yield call(doGetReelByRandom, isLoadMore);
    return { result, error };
  }
}

export function* getReelsFn(action: PayloadAction<IActionGetReelsPayload>) {
  const { isLoadMore = false, onSuccess, onFail } = action.payload;
  const { result, error } = yield call(doGetReel, isLoadMore);
  if (result) onSuccess?.(result);
  else onFail?.(error);
}
