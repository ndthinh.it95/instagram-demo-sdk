import { IPagination } from '../../../pagination';
import { IActionCallback } from '../../../redux/base';
import { IPostReel, IReel } from '../models';

export const ROOT_MODULE = 'post';
export const GET_POSTS = `${ROOT_MODULE}/GET_POSTS`;
export const GET_REELS = `${ROOT_MODULE}/GET_REELS`;

export interface IPostsState {
  username_or_id_or_url?: string;
  isFetchPosts: boolean;
  posts: IPagination<IPostReel>;
  reels: IPagination<IReel>;
}

// Payload
export interface IActionSavePostUsernameQueryPayload extends IActionCallback {
  username_or_id_or_url: string;
}

export interface IActionGetPostsPayload extends IActionCallback {
  isLoadMore?: boolean;
}
export interface IActionSavePostsPayload extends IActionCallback {
  posts: IPagination<IPostReel>;
}

export interface IActionGetReelsPayload extends IActionCallback {
  isLoadMore?: boolean;
}
export interface IActionSaveReelsPayload extends IActionCallback {
  reels: IPagination<IReel>;
}

export interface IActionSaveIsFetchPostsPayload extends IActionCallback {
  isFetchPosts: boolean;
}
