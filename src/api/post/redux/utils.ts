import { IPagination } from '../../../pagination';
import { IPostReel, IReel } from '../models';

type FormatDataType = IPostReel | IReel;

export const formatListPosts = (
  oldPage: IPagination<FormatDataType>,
  newPage: IPagination<FormatDataType>,
  isLoadMore = false,
): { result: IPagination<FormatDataType> } => {
  const newList = [...newPage.data.items];
  return {
    result: {
      ...newPage,
      data: {
        ...newPage.data,
        items: isLoadMore ? [...oldPage.data.items, ...newList] : newList,
      },
    },
  };
};
