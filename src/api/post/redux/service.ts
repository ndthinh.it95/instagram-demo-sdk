import { IPagination } from '../../../pagination';
import { Api } from '../../index';
import { IPostReel, IReel } from '../models';

export const getPosts = async (
  username_or_id_or_url: string,
  pagination_token?: string,
): Promise<{
  result?: IPagination<IPostReel>;
  error?: unknown;
}> => {
  try {
    const result = await Api.instance().post.getPostReel(username_or_id_or_url, pagination_token);
    return { result };
  } catch (error) {
    console.error('error', error);
    return { error };
  }
};

export const getReels = async (
  username_or_id_or_url: string,
  pagination_token?: string,
): Promise<{
  result?: IPagination<IReel>;
  error?: unknown;
}> => {
  try {
    const result = await Api.instance().post.getReel(username_or_id_or_url, pagination_token);
    return { result };
  } catch (error) {
    console.error('error', error);
    return { error };
  }
};
