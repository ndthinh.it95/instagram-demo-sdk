import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  IActionSaveIsFetchPostsPayload,
  IActionSavePostsPayload,
  IActionSavePostUsernameQueryPayload,
  IActionSaveReelsPayload,
  IPostsState,
} from './models';
import { createSelector } from 'reselect';
import { resetAllState } from '../../../redux/base/actions';

export const initialPosts = {
  data: {
    count: 0,
    items: [],
  },
  pagination_token: undefined,
};

const initialState: IPostsState = {
  username_or_id_or_url: undefined,
  isFetchPosts: false,
  posts: initialPosts,
  reels: initialPosts,
};

const postStateSlice = createSlice({
  name: 'post',
  initialState,
  reducers: {
    SAVE_POSTS(state: IPostsState, action: PayloadAction<IActionSavePostsPayload>) {
      const { posts } = action.payload;
      return { ...state, posts, isFetchPosts: true };
    },
    SAVE_REELS(state: IPostsState, action: PayloadAction<IActionSaveReelsPayload>) {
      const { reels } = action.payload;
      return { ...state, reels };
    },
    SAVE_POST_USERNAME_QUERY(state: IPostsState, action: PayloadAction<IActionSavePostUsernameQueryPayload>) {
      const { username_or_id_or_url } = action.payload;
      return { ...state, username_or_id_or_url };
    },
    SAVE_IS_FETCH_POSTS(state: IPostsState, action: PayloadAction<IActionSaveIsFetchPostsPayload>) {
      const { isFetchPosts } = action.payload;
      return { ...state, isFetchPosts };
    },
  },
  extraReducers: (builder) => builder.addCase(resetAllState, () => initialState),
});

export const {
  SAVE_POSTS: savePosts,
  SAVE_REELS: saveReels,
  SAVE_POST_USERNAME_QUERY: savePostUsernameQuery,
  SAVE_IS_FETCH_POSTS: saveIsFetchPosts,
} = postStateSlice.actions;

export const { reducer } = postStateSlice;

// Selector state
export const selectPostsRoot = (state: { post: IPostsState }): IPostsState => state.post;

export const selectIsFetchPosts = createSelector([selectPostsRoot], (state) => state.isFetchPosts);
export const selectListPost = createSelector([selectPostsRoot], (state) => state.posts);
export const selectListReel = createSelector([selectPostsRoot], (state) => state.reels);
export const selectPostUsernameQuery = createSelector([selectPostsRoot], (state) => state.username_or_id_or_url);
