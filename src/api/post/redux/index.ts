import { reducer as postStateSlice } from './reducer';
import { createAction } from '@reduxjs/toolkit';
import { takeLatest } from 'redux-saga/effects';
import * as Models from './models';
import * as FuncSaga from './saga';

export const getPosts = createAction<Models.IActionGetPostsPayload>(Models.GET_POSTS);
export const getReels = createAction<Models.IActionGetReelsPayload>(Models.GET_REELS);

function* postSaga() {
  yield takeLatest(getPosts, FuncSaga.getPostsFn);
  yield takeLatest(getReels, FuncSaga.getReelsFn);
}

export { postSaga, postStateSlice };
