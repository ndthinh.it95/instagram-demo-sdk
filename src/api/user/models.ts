export interface IUser {
  id: string;
  username: string;
  full_name: string;
  is_private: boolean;
  is_verified: boolean;
  profile_pic_url?: string;
  profile_pic_url_hd?: string;
}
