import { IPagination } from '../../../pagination';
import { IActionCallback } from '../../../redux/base';
import { IUser } from '../models';

export const ROOT_MODULE = 'user';
export const GET_USERS = `${ROOT_MODULE}/GET_USERS`;
export const GET_RANDOM_USER = `${ROOT_MODULE}/GET_RANDOM_USER`;
export const GET_NEXT_RANDOM_USER = `${ROOT_MODULE}/GET_NEXT_RANDOM_USER`;

export interface IUserState {
  search_query?: string;
  users: IPagination<IUser>;
  randomUser?: IUser;
  nextRandomUser?: IUser;
}

// Payload
export interface IActionSaveSearchQueryPayload extends IActionCallback {
  search_query: string;
}

export type IActionGetUsersPayload = IActionCallback;
export type IActionSaveUsersPayload = IActionCallback & {
  users: IPagination<IUser>;
};

export type IActionGetRandomUserPayload = IActionCallback;
export type IActionSaveRandomUserPayload = IActionCallback & {
  randomUser: IUser;
};

export type IActionGetNextRandomUserPayload = IActionCallback;
export type IActionSaveNextRandomUserPayload = IActionCallback & {
  nextRandomUser: IUser;
};
