import { Api } from '../../index';
import { IPagination } from '../../../pagination';
import { IUser } from '../models';

export const getUsers = async (
  search_query: string,
): Promise<{
  result?: IPagination<IUser>;
  error?: unknown;
}> => {
  try {
    const result = await Api.instance().user.getUsers(search_query);
    return { result };
  } catch (error) {
    console.error('error', error);
    return { error };
  }
};

export const getRandomUser = async (): Promise<{
  result?: IUser;
  error?: unknown;
}> => {
  try {
    const result = await Api.instance().user.getRandomUser();
    return { result };
  } catch (error) {
    console.error('error', error);
    return { error };
  }
};
