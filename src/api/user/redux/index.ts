import { reducer as userStateSlice } from './reducer';
import { createAction } from '@reduxjs/toolkit';
import { takeLatest } from 'redux-saga/effects';
import * as Models from './models';
import * as FuncSaga from './saga';

export const getUsers = createAction<Models.IActionGetUsersPayload>(Models.GET_USERS);
export const getRandomUser = createAction<Models.IActionGetRandomUserPayload>(Models.GET_RANDOM_USER);
export const getNextRandomUser = createAction<Models.IActionGetNextRandomUserPayload>(Models.GET_NEXT_RANDOM_USER);

function* userSaga() {
  yield takeLatest(getUsers, FuncSaga.getUsersFn);
  yield takeLatest(getRandomUser, FuncSaga.getRandomUserFn);
  yield takeLatest(getNextRandomUser, FuncSaga.getNextRandomUserFn);
}

export { userSaga, userStateSlice };
