import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  IActionSaveNextRandomUserPayload,
  IActionSaveRandomUserPayload,
  IActionSaveSearchQueryPayload,
  IActionSaveUsersPayload,
  IUserState,
} from './models';
import { createSelector } from 'reselect';
import { resetAllState } from '../../../redux/base/actions';

export const initialUser = {
  data: {
    count: 0,
    items: [],
  },
  pagination_token: undefined,
};

const initialState: IUserState = {
  search_query: undefined,
  users: initialUser,
  randomUser: undefined,
  nextRandomUser: undefined,
};

const userStateSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    SAVE_SEARCH_QUERY(state: IUserState, action: PayloadAction<IActionSaveSearchQueryPayload>) {
      const { search_query } = action.payload;
      return { ...state, search_query };
    },
    SAVE_USERS(state: IUserState, action: PayloadAction<IActionSaveUsersPayload>) {
      const { users } = action.payload;
      return { ...state, users };
    },
    SAVE_RANDOM_USER(state: IUserState, action: PayloadAction<IActionSaveRandomUserPayload>) {
      const { randomUser } = action.payload;
      return { ...state, randomUser };
    },
    SAVE_NEXT_RANDOM_USER(state: IUserState, action: PayloadAction<IActionSaveNextRandomUserPayload>) {
      const { nextRandomUser } = action.payload;
      return { ...state, nextRandomUser };
    },
  },
  extraReducers: (builder) => builder.addCase(resetAllState, () => initialState),
});

export const {
  SAVE_SEARCH_QUERY: saveSearchQuery,
  SAVE_USERS: saveUsers,
  SAVE_RANDOM_USER: saveRandomUser,
  SAVE_NEXT_RANDOM_USER: saveNextRandomUser,
} = userStateSlice.actions;

export const { reducer } = userStateSlice;

// Selector state
export const selectUserRoot = (state: { user: IUserState }): IUserState => state.user;

export const selectListUsers = createSelector([selectUserRoot], (state) => state.users);
export const selectSearchQuery = createSelector([selectUserRoot], (state) => state.search_query);
export const selectRandomUser = createSelector([selectUserRoot], (state) => state.randomUser);
export const selectNextRandomUser = createSelector([selectUserRoot], (state) => state.nextRandomUser);
