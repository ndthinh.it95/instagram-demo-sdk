import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select } from 'redux-saga/effects';
import { IActionGetNextRandomUserPayload, IActionGetRandomUserPayload, IActionGetUsersPayload } from './models';
import * as UserService from './service';
import { initialUser, saveNextRandomUser, saveRandomUser, saveUsers, selectUserRoot } from './reducer';

function* selectSearch() {
  const { search_query } = yield select(selectUserRoot);
  return { search_query };
}

export function* doGetRandomUser() {
  const { result, error } = yield call(UserService.getRandomUser);
  if (result) {
    yield put(saveRandomUser({ randomUser: result }));
  }
  return { result, error };
}

export function* doGetNextRandomUser() {
  const { result, error } = yield call(UserService.getRandomUser);
  if (result) {
    yield put(saveNextRandomUser({ nextRandomUser: result }));
  }
  return { result, error };
}

export function* getUsersFn(action: PayloadAction<IActionGetUsersPayload>) {
  const { onSuccess, onFail } = action.payload;
  const { search_query } = yield call(selectSearch);
  if (!search_query) {
    yield put(saveUsers({ users: initialUser }));
    onSuccess?.(initialUser);
  } else {
    const { result, error } = yield call(UserService.getUsers, search_query);
    if (result) {
      yield put(saveUsers({ users: result }));
      onSuccess?.(result);
    } else onFail?.(error);
  }
}

export function* getRandomUserFn(action: PayloadAction<IActionGetRandomUserPayload>) {
  const { onSuccess, onFail } = action.payload;
  const { result, error } = yield call(doGetRandomUser);
  if (result) onSuccess?.(result);
  else onFail?.(error);
}

export function* getNextRandomUserFn(action: PayloadAction<IActionGetNextRandomUserPayload>) {
  const { onSuccess, onFail } = action.payload;
  const { result, error } = yield call(doGetNextRandomUser);
  if (result) onSuccess?.(result);
  else onFail?.(error);
}
