const getRandomCharacter = (): string => {
  const letters = 'abcdefghijklmnopqrstuvwxyz';
  const randomIndex = Math.floor(Math.random() * letters.length);
  return letters[randomIndex];
};

export { getRandomCharacter };
