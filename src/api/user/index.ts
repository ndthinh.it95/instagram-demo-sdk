import { ApiClient } from '../../http/client';
import { IPagination } from '../../pagination';
import { IUser } from './models';
import { getRandomCharacter } from './utils';

export class User {
  public constructor(public readonly client: ApiClient) {}

  public async getUsers(search_query: string): Promise<IPagination<IUser>> {
    return this.client.get<IPagination<IUser>>('/v1/search_users', { search_query });
  }

  public async getRandomUser(): Promise<IUser> {
    const search_query = getRandomCharacter();
    const users = await this.client.get<IPagination<IUser>>('/v1/search_users', { search_query });
    return users.data.items[0];
  }
}
