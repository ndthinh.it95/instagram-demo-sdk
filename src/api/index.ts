import { ApiClient } from '../http/client';
import { ApiConfiguration } from './models';
import { Post } from './post';
import { User } from './user';

export const createApiClient = (config: ApiConfiguration): ApiClient => {
  const client = new ApiClient(config);
  return client;
};

export class Api {
  private static _instance?: Api;
  public readonly user: User;
  public readonly post: Post;

  static instance(config?: ApiConfiguration): Api {
    if (!Api._instance) {
      if (!config) {
        throw new Error('Missing config for first initialization.');
      }
      const client = createApiClient(config);
      Api._instance = new Api(client);
    }
    return Api._instance;
  }

  public constructor(public readonly client: ApiClient) {
    this.user = new User(client);
    this.post = new Post(client);
  }
}
